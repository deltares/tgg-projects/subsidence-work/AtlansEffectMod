module AtlansEffectMod

using Atlans
using NCDatasets
using Dates


function prepare_subsoil_data(ds, path_csv, ymin_idx, ymax_idx)
	df = Atlans.read_params_table(path_csv)
	tables = Atlans.build_lookup_tables(df)

	data = Dict{Symbol, Array}()
	for key in keys(ds)
		if ndims(ds[key]) == 1
			if key == "y"
				data[Symbol(key)] = ds[key][ymin_idx:ymax_idx]
			else
				data[Symbol(key)] = ds[key][:]
			end
		elseif ndims(ds[key]) == 2
			data[Symbol(key)] = ds[key][:, ymin_idx:ymax_idx]
		elseif ndims(ds[key]) == 3
			data[Symbol(key)] = ds[key][:, :, ymin_idx:ymax_idx]
		end
	end
	return Atlans.SubsoilData(data, tables)
end


function create_model(
	groundwater::Type,
	consolidation::Type,
	oxidation::Type,
	preconsolidation::Type,
	shrinkage::Type,
	adaptive_cellsize,
	timestepper,
	subsoil,
)
	x = subsoil.data[:x]
	y = subsoil.data[:y]
	base = subsoil.data[:zbase]
	domainbase = subsoil.data[:domainbase]
	surface = subsoil.data[:surface_level]
	phreatic = subsoil.data[:phreatic_level]
	geology = subsoil.data[:geology]
	lithology = subsoil.data[:lithology]
	thickness = subsoil.data[:thickness]

	columntype = Atlans.SoilColumn{groundwater, consolidation, preconsolidation, oxidation, shrinkage}
	columns = Vector{columntype}()
	index = Vector{CartesianIndex}()

	for I in CartesianIndices(domainbase)
		(ismissing(domainbase[I]) || ismissing(surface[I]) || ismissing(phreatic[I])) &&
			continue
		try
			domain = Atlans.prepare_domain(
				domainbase[I],
				base[I],
				surface[I],
				thickness[:, I],
				adaptive_cellsize.Δzmax,
				geology[:, I],
				lithology[:, I],
			)

			length(domain.z) == 0 && continue

			g_column = Atlans.initialize(groundwater, domain, subsoil, I)
			c_column = Atlans.initialize(consolidation, preconsolidation, domain, subsoil, I)
			o_column = Atlans.initialize(oxidation, domain, subsoil, I)
			s_column = Atlans.initialize(shrinkage, domain, subsoil, I)

			column = Atlans.SoilColumn(
				domain.zbottom,
				x[I[1]],
				y[I[2]],
				domain.z,
				domain.Δz,
				g_column,
				c_column,
				o_column,
				s_column,
			)
			# Set values such as preconsolidation stress, τ0, etc.
			# This requires groundwater: pore pressure, etc.
			Atlans.apply_preconsolidation!(column)
			push!(columns, column)
			push!(index, I)
		catch e
			if isa(e, MethodError)
				continue
			else
				rethrow(e)
			end
		end
	end

	shape = size(domainbase)
	fillnan() = fill(NaN, shape)

	output = Atlans.Output(x, y, fillnan(), fillnan(), fillnan(), fillnan(), fillnan(), fillnan())

	return Atlans.Model(columns, index, timestepper, adaptive_cellsize, output)
end


function update_glg!(subsoil, glg)
	subsoil.data[:phreatic_level] = glg
end


end # module AtlansEffectMod
