# AtlansEffectMod

This project is aimed to be able to run a workflow for the "Effect module bodemdaling" which consist of a combined `Julia` and `Python` workflow. Subsidence calculations are done using an a slightly altered implementation of [Atlantis Julia](https://gitlab.com/deltares/subsidence/atlans.jl) and the post-processing is done using a set of Python scripts.

Snakemake is used for workflow management which determines the order in which the scripts are called. The workflow is contained in the [Snakefile](./Snakefile)

## Prequisites

To be able to run this project Python and Julia are required. Julia can be installed from the `Microsoft store` directly in *Windows Powershell*:

```powershell
winget install julia -s msstore
```

Pixi is used [pixi](https://prefix.dev/) to install a Python environment and all the required dependencies.

Download & install pixi with the command in *Windows Powershell*:

```powershell
iwr -useb https://pixi.sh/install.ps1 | iex
```

## Installing all requirements to run the project

First, clone the project onto your local machine:

```powershell
git clone https://gitlab.com/deltares/tgg-projects/subsidence-work/AtlansEffectMod.git
```

To install everything that is required to run the project, the [pixi.toml](./pixi.toml) contains a task `install-project` that install the Python and Julia dependencies. Run the task by:

```powershell
pixi run install-project
```

## Running the project

The [pixi.toml](./pixi.toml) also contains a task (`project`) to run the complete workflow. This calls the [Snakefile](./Snakefile) which then runs the scripts in the correct order and produces the output. Run the task by:

```powershell
pixi run project
```

## Run project on the WCF node (Deltares)

Pixi encounters some problems with the WCF node that has to do with the Windows paths of the virtual machines. It is also possible to run the workflow manually. Then the Python dependencies need to be installed before running the Snakefile. The project contains a [python_env.yml](./python_env.yml) to install a working Python environment with `Anaconda`, for example in `miniforge prompt`. Navigate to the project folder and install the environment by:

```powershell
cd [PATH TO PROJECT]
mamba env create -f python_env.yml
```

> [!NOTE]
>
> The command is assuming that `mamba` is available to use for installation. If this is not the
> case, `conda` can be used.

 Additionally the Snakefile has to be run with `miniforge prompt`. The Julia environment has already been correctly installed by Pixi. Simply run the entire workflow manually by:

 ```powershell
 snakemake -c1
 ```
