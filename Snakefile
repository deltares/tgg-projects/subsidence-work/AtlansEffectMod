import os
import numpy as np
from pathlib import Path

os.environ["JULIA_PROJECT"] = "."
os.environ["JULIA_NUM_THREADS"] = "auto"

envvars:
    "JULIA_PROJECT",
    "JULIA_NUM_THREADS",


project_dir = Path(r"p:\11211541-003-kentalleneffecten\Bodemdaling")


scenarios = ['REF2017VP', 'S2050VP']
glg_types = ['mean', 'mean', 'max', 'zwopgave']

for scenario in scenarios:
    scenario_folder = project_dir/rf'output_dpzw2017_new/{scenario}'
    loose_file_folder = project_dir/rf'output_dpzw2017_new/{scenario}/partitions'
    scenario_folder.mkdir(exist_ok=True)
    loose_file_folder.mkdir(exist_ok=True)

chunksize = 100

rule all:
    input:
        path_nc=expand(
            project_dir/r'output_dpzw2017_new/{scenario}/{scenario}_{glg_type}.nc',
            scenario=scenarios,
            glg_type=glg_types
        )


rule run_atlantis:
    input:
        subsurface_model=project_dir/r"input/atlans_subsurface_model_incl_flevoland.nc",
        param_table=project_dir/r"input/parameters.csv",
        glg=project_dir/r"input/glgs/DPZW2017/glg_{scenario}_{glg_type}.nc",
        temperature=project_dir/r"input/temperatures/DPZW2017/temperature_forcing_{scenario}.csv"
    params:
        chunksize=chunksize,
        project_dir=project_dir/"output_dpzw2017_new"
    output:
        output=project_dir/r"output_dpzw2017_new/{scenario}/partitions/{scenario}_{glg_type}_1.nc"
    script: "work/run_atlantis.jl"


rule merge:
    input:
        files=project_dir/r"output_dpzw2017_new/{scenario}/partitions/{scenario}_{glg_type}_1.nc"
    output:
        output=project_dir/r"output_dpzw2017_new/{scenario}/partitions/{scenario}_{glg_type}.nc"
    script: "work/merge_output.py"


rule move:
    input:
        nc_file=project_dir/r"output_dpzw2017_new/{scenario}/partitions/{scenario}_{glg_type}.nc",
    output:
        nc_file=project_dir/r"output_dpzw2017_new/{scenario}/{scenario}_{glg_type}.nc"
    script: "work/move_output.py"