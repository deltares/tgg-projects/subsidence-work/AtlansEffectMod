import shutil
from pathlib import Path


input_file = snakemake.input["nc_file"]
output_file = snakemake.output["nc_file"]

output_nc = Path(input_file)
partition_dir = output_nc.parents[0]
scenario = output_nc.stem

shutil.move(input_file, output_file)

files_to_remove = [f for f in partition_dir.glob('*.nc') if scenario in str(f)]

for f in files_to_remove:
    Path(f).unlink()
