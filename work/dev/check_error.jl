using Atlans
using Dates
using NCDatasets
using AtlansEffectMod: prepare_subsoil_data, create_model, update_glg!


function get_ychunk_indices(subsurface_model, chunk_size)
    min_indices = Vector{Int64}()
    max_indices = Vector{Int64}()

    ylength = length(subsurface_model[:y])
    
    idx_min = 1
    while idx_min < ylength
        idx_max = idx_min + chunk_size
        push!(min_indices, idx_min)
        
        if idx_max < ylength
            push!(max_indices, idx_max)
        else
            push!(max_indices, ylength)
        end
        idx_min = idx_max + 1
    end
    return min_indices, max_indices
end


path_subsurface_model = raw"n:\Projects\11209000\11209259\B. Measurements and calculations\009 effectmodule bodemdaling\data\3-input\atlans_subsurface_model_incl_flevoland.nc"
path_table = raw"n:\Projects\11209000\11209259\B. Measurements and calculations\009 effectmodule bodemdaling\data\3-input\parameters.csv"
path_glgs = raw"n:\Projects\11209000\11209259\B. Measurements and calculations\009 effectmodule bodemdaling\data\3-input\test_run_nederland\glg_REF2017BP18_1_mean.nc"
path_temperature = raw"n:\Projects\11209000\11209259\B. Measurements and calculations\009 effectmodule bodemdaling\data\3-input\test_run_nederland\temperature_forcing_REF2017BP18.csv"
outputpath = raw"n:\Projects\11209000\11209259\B. Measurements and calculations\009 effectmodule bodemdaling\data\3-input\temp.nc"

var_name = "__xarray_dataarray_variable__"
subsurface_model = Dataset(path_subsurface_model)
glg_nc = Dataset(path_glgs)

min_indices, max_indices = get_ychunk_indices(subsurface_model, 100)

min_idx = 2627
max_idx = 2727

subsoil = prepare_subsoil_data(subsurface_model, path_table, min_idx, max_idx)

forcing = (temperature = Atlans.Temperature(path_temperature),)

# glg = glg_nc[var_name][:, min_idx:max_idx]

# update_glg!(subsoil, glg)

subsoil.data[:phreatic_level] = subsoil.data[:surface_level] .- 2.5 # TODO: remove this line when GLG input is correct

@show "Create model"
# model = create_model(
#     Atlans.HydrostaticGroundwater,
#     Atlans.DrainingAbcIsotache,
#     Atlans.CarbonStore,
#     Atlans.OverConsolidationRatio,
#     Atlans.NullShrinkage,
#     Atlans.AdaptiveCellsize(0.25, 0.01),
#     Atlans.ExponentialTimeStepper(1.0, 2),
#     subsoil
# )   


groundwater = Atlans.HydrostaticGroundwater
consolidation = Atlans.DrainingAbcIsotache
oxidation = Atlans.CarbonStore
preconsolidation = Atlans.OverConsolidationRatio
shrinkage = Atlans.NullShrinkage
adaptive_cellsize = Atlans.AdaptiveCellsize(0.25, 0.01)

x = subsoil.data[:x]
y = subsoil.data[:y]
base = subsoil.data[:zbase]
domainbase = subsoil.data[:domainbase]
surface = subsoil.data[:surface_level]
phreatic = subsoil.data[:phreatic_level]
geology = subsoil.data[:geology]
lithology = subsoil.data[:lithology]
thickness = subsoil.data[:thickness]

columntype = Atlans.SoilColumn{groundwater,consolidation,preconsolidation,oxidation,shrinkage}
columns = Vector{columntype}()
index = Vector{CartesianIndex}()

for I in CartesianIndices(domainbase)
    @show I
    (ismissing(domainbase[I]) || ismissing(surface[I]) || ismissing(phreatic[I])) &&
        continue
    try
        domain = Atlans.prepare_domain(
            domainbase[I],
            base[I],
            surface[I],
            thickness[:, I],
            adaptive_cellsize.Δzmax,
            geology[:, I],
            lithology[:, I],
        )
        
        length(domain.z) == 0 && continue

        g_column = Atlans.initialize(groundwater, domain, subsoil, I)
        c_column = Atlans.initialize(consolidation, preconsolidation, domain, subsoil, I)
        o_column = Atlans.initialize(oxidation, domain, subsoil, I)
        s_column = Atlans.initialize(shrinkage, domain, subsoil, I)

        column = Atlans.SoilColumn(
            domainbase[I],
            x[I[1]],
            y[I[2]],
            domain.z,
            domain.Δz,
            g_column,
            c_column,
            o_column,
            s_column,
        )
        # Set values such as preconsolidation stress, τ0, etc.        
        # This requires groundwater: pore pressure, etc.
        Atlans.apply_preconsolidation!(column)
        push!(columns, column)
        push!(index, I)
    catch e
        if isa(e, MethodError)
            continue
        else
            rethrow(e)
        end
    end
end

shape = size(domainbase)
fillnan() = fill(NaN, shape)

output = Atlans.Output(x, y, fillnan(), fillnan(), fillnan(), fillnan(), fillnan(),  fillnan())
@show "Create simulation"
# simulation = Atlans.Simulation(
#     model,
#     outputpath,
#     DateTime("2028-01-01"),
#     forcing,
# )

# Atlans.run!(simulation)

##

domainbase = domainbase[I]
modelbase = base[I]
surface = surface[I]
thickness = thickness[:, I]
Δzmax = adaptive_cellsize.Δzmax
geology = geology[:, I]
lithology = lithology[:, I]


thickness = filter(!ismissing, thickness)
geology = filter(!ismissing, geology)
lithology = filter(!ismissing, lithology)


ztop = modelbase .+ cumsum(thickness)
zbot = ztop .- thickness

base_index = findfirst(domainbase .< skipmissing(ztop))
top_index = findlast(surface .> skipmissing(zbot))

Δz = thickness[base_index:top_index]
index, ncells = discretize(Δz, Δzmax)
Δz = (Δz./ncells)[index]
z = zbot[base_index] .+ cumsum(Δz) .- 0.5 .* Δz
n = sum(ncells)
index .+= (base_index - 1)
