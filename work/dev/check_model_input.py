import numpy as np
import xarray as xr
from pathlib import Path


workdir = Path(r'n:\Projects\11209000\11209259\B. Measurements and calculations\009 effectmodule bodemdaling\data')

xmin, xmax = 231_000, 233_000
ymin, ymax = 523_000, 525_000

model = xr.open_dataset(workdir/r'3-input/atlans_subsurface_model.nc')

sel = model.sel(x=slice(xmin, xmax), y=slice(ymax, ymin))
print(2)