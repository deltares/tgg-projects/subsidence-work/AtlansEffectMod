using Atlans
using Dates

Δzmax = 0.25

workdir = raw"c:\Users\knaake\OneDrive - Stichting Deltares\Documents\projects\bugfix_atlantis"

path_subsurface_model = joinpath(workdir, "data", "subsurface_model.nc")
path_table = joinpath(workdir, "data", "parameters.csv")
path_temperature = joinpath(workdir, "data", "temperature_forcing_REF2017VP.csv")

model = Atlans.Model(
	Atlans.HydrostaticGroundwater,
	Atlans.DrainingAbcIsotache,
	Atlans.CarbonStore,
	Atlans.OverConsolidationRatio,
	Atlans.NullShrinkage,
	Atlans.AdaptiveCellsize(0.25, 0.01),
	Atlans.ExponentialTimeStepper(1.0, 2),
	path_subsurface_model,
	path_table,
)

forcings = Atlans.Forcings(temperature = Atlans.Temperature(path_temperature))

simulation = Atlans.Simulation(
	model,
	joinpath(workdir, "results", "effectmodule_test_in_environment2.nc"),
	DateTime("2028-01-01"),
	forcings = forcings,
)

Atlans.run!(simulation)
