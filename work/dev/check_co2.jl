using Atlans
using Dates
using NCDatasets
using DataFrames
using AtlansEffectMod: prepare_subsoil_data, create_model, update_glg!

Δzmax = 0.25

workdir = raw"c:\Users\knaake\OneDrive - Stichting Deltares\Documents\projects\effectmodule\check_co2"

var_name = "__xarray_dataarray_variable__"

subsurface_model = Dataset(joinpath(workdir, "co2_columns_check.nc"))
path_table = joinpath(workdir, "parameters.csv")
path_temperature = joinpath(workdir, "temperature_forcing_REF2017VP.csv")
glg = Dataset(joinpath(workdir, "glg.nc"))

subsoil = prepare_subsoil_data(subsurface_model, path_table, 1, 2)
forcing = (temperature = Atlans.Temperature(path_temperature),)


update_glg!(subsoil, glg[var_name][:, 1:2])

model = create_model(
	Atlans.HydrostaticGroundwater,
	Atlans.DrainingAbcIsotache,
	Atlans.CarbonStore,
	Atlans.OverConsolidationRatio,
	Atlans.NullShrinkage,
	Atlans.AdaptiveCellsize(0.25, 0.01),
	Atlans.ExponentialTimeStepper(1.0, 2),
	subsoil,
)

simulation = Atlans.Simulation(
	model,
	joinpath(workdir, "output.nc"),
	DateTime("2028-01-01"),
	forcing,
)

Atlans.advance_forcingperiod!(simulation)
# @show model.output.carbon_loss
