using Atlans
using Dates
using NCDatasets
using AtlansEffectMod: prepare_subsoil_data, create_model, update_glg!


function get_ychunk_indices(subsurface_model, chunk_size)
	min_indices = Vector{Int64}()
	max_indices = Vector{Int64}()

	ylength = length(subsurface_model[:y])

	idx_min = 1
	while idx_min < ylength
		idx_max = idx_min + (chunk_size - 1)
		push!(min_indices, idx_min)

		if idx_max < ylength
			push!(max_indices, idx_max)
		else
			push!(max_indices, ylength)
		end
		idx_min = idx_max + 1
	end
	return min_indices, max_indices
end


function get_scenario_from_glg(glg_path)
	replace(splitext(basename(glg_path))[1], "glg_" => "")
end


path_subsurface_model = snakemake.input["subsurface_model"]
path_table = snakemake.input["param_table"]
path_glgs = snakemake.input["glg"]
path_temperature = snakemake.input["temperature"]

chunksize = snakemake.params["chunksize"]
project_dir = snakemake.params["project_dir"]

scenario = get_scenario_from_glg(path_glgs)
@show scenario
output_dir = joinpath(project_dir, "$(split(scenario, "_")[1])", "partitions")

var_name = "__xarray_dataarray_variable__"
subsurface_model = Dataset(path_subsurface_model)
glg_nc = Dataset(path_glgs)

min_indices, max_indices = get_ychunk_indices(subsurface_model, chunksize)

for (ii, (min_idx, max_idx)) in enumerate(zip(min_indices, max_indices))
	outputpath = joinpath(output_dir, "$(scenario)_$(ii).nc")
	@show ii, min_idx, max_idx
	@show outputpath

	subsoil = prepare_subsoil_data(subsurface_model, path_table, min_idx, max_idx)

	forcings = Atlans.Forcings(temperature = Atlans.Temperature(path_temperature))

	glg = glg_nc[var_name][:, min_idx:max_idx]

	update_glg!(subsoil, glg)

	model = create_model(
		Atlans.HydrostaticGroundwater,
		Atlans.DrainingAbcIsotache,
		Atlans.CarbonStore,
		Atlans.OverConsolidationRatio,
		Atlans.NullShrinkage,
		Atlans.AdaptiveCellsize(0.25, 0.01),
		Atlans.ExponentialTimeStepper(1.0, 2),
		subsoil,
	)

	simulation = Atlans.Simulation(
		model,
		outputpath,
		DateTime("2028-01-01"),
		forcings=forcings,
	)

	Atlans.run!(simulation)
end
