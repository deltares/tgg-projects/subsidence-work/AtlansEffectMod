import xarray as xr
from dask.diagnostics import ProgressBar
from pathlib import Path


CHUNK_SIZE = {'time': 8, 'x': 2700, 'y': 100}
COMPRESSION = {
    'phreatic_level': {'zlib': True, 'complevel': 9},
    'consolidation': {'zlib': True, 'complevel': 9},
    'oxidation': {'zlib': True, 'complevel': 9},
    'shrinkage': {'zlib': True, 'complevel': 9},
    'subsidence': {'zlib': True, 'complevel': 9},
    'carbon_loss': {'zlib': True, 'complevel': 9},
}


def combine_subsidence_netcdfs(nc_files, outputname):
    datasets = [xr.open_dataset(nc).chunk(CHUNK_SIZE) for nc in nc_files]
    combined = xr.concat(datasets, dim='y')

    output = combined.to_netcdf(
        outputname, engine='h5netcdf', encoding=COMPRESSION, compute=False
    )

    with ProgressBar():
        output.compute()

    for ds in datasets:
        ds.close()


output_nc = snakemake.output["output"]

output_nc = Path(output_nc)
partition_dir = output_nc.parents[0]
scenario = output_nc.stem

ncs_to_combine = [f for f in partition_dir.glob('*.nc') if scenario in str(f)]

combine_subsidence_netcdfs(ncs_to_combine, output_nc)
